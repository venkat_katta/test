exports.config = {
  seleniumAddress: "http://127.0.0.1:4444/wd/hub",
  SELENIUM_PROMISE_MANAGER: false,
  capabilities: {
    browserName: "chrome",
    chromeOptions: {
      useAutomationExtension: false,
      args: [
        "--disable-extensions",
        "--disable-gpu",
        "--disable-dev-shm-usage",
        "--no-sandbox",
        "--disable-infobars"
        // "--headless"
      ]
    }
  },
  plugins: [
    {
      package: "protractor-multiple-cucumber-html-reporter-plugin",
      options: {
        automaticallyGenerateReport: true,
        removeExistingJsonReportFile: true,
        reportName: "Protarctor-config",
        pageTitle: "Protarctor-config - Results",
        reportPath: "./report",
        openReportInBrowser: true
      }
    }
  ],
  directConnect: true,
  // baseUrl: "https://www.google.com",
  framework: "custom",
  frameworkPath: require.resolve("protractor-cucumber-framework"),
  ignoreUncaughtExceptions: true,
  specs: ["./features/*.feature"],

  onPrepare: () => {
    browser.ignoreSynchronization = true;
    browser
      .manage()
      .window()
      .maximize();
    browser.driver.manage().deleteAllCookies();
  },
  cucumberOpts: {
    compiler: "ts:ts-node/register",
    format: ["json:.tmp/reports/results.json"],
    require: ["./dist/stepdefinitions/*.js", "./dist/support/*.js"],
    strict: true,
    tags: false
    // "@CucumberScenario or @ProtractorScenario or @TypeScriptScenario or @OutlineScenario"
  }
};
